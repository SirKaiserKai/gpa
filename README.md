# GPA #

The provided problem, finding the averages of a student data set for several traits, is ideal for a map reduce job. Due to personal familiarity and time constraints the application was written in the Go programming with the parallel computing package [Glow](https://github.com/chrislusf/glow). The code base is heavily commented in case the reader lacks familiarity with the Go programming language. 

### Data Loading ###

The data set consists of 1 million student records on an S3 instance. Within the codebase the 'input' package handles querying S3 and relaying the student JSON objects to the main process which will handle computing the averages. The data loading process begins by querying for the file names from the folder which contains the student's data. Following this, it spawns several [goroutines](https://www.golang-book.com/books/intro/10) (separate processes) which will download the individual files. Operating in a 'fan-in' manner, the students are sent to a single [channel](https://tour.golang.org/concurrency/2) which feeds into a map function in the main process. 

### Data Processing ###

Running in the main process is a chain of functions which handle the processing of the student data into a set of averages. The data sets are sent along 3 partitions for processing. The student struct is split into 3 Average structs, each holding the student's GPA value, the key (their major, gender, or the total label), and a TupleCount field which is set to 1. The Average Structs will be reduced by summing the GPA and TupleCount fields sorted by their key values. Following this, each of the summed Average structs have the GPA field divided by the TupleCount field to get the average for each key. Finally, the structs are passed in the 'terminal' channel in order to be added to the output map (dictionary). 

See diagram below for a visualization of the process.
![](https://i.imgur.com/SP3PS7U.png)

### Output ###

The main method also hosts a web server which displays a JSON object with the output on the '/averages' path with default port of '8082'. (e.g. www.domain.com:8082/averages)

### How do I get set up? ###

**Docker**
Prerequisites:
In the Dockerfile you'll need to replace the default values of the AWS environment variables:

	ENV AWS_REGION="us-east-1"
	ENV AWS_ACCESS_KEY_ID="access_key"
	ENV AWS_SECRET_ACCESS_KEY="secret_access_key"

Then use `make`:	

	# builds the docker image
    make docker-build
	
	# runs the docker image
	make docker-run

**Without Docker**

You can run it without docker, however, there are certain prerequisites:

	1. Have Go installed
	2. mkdir -p ~/go/src/github.com/sirkaiserkai 
	3. mv gpa ~/go/src/github.com/sirkaiserkai 
	4. cd ~/go/src/github.com/sirkaiserkai

Following this you can leverage the Makefile by running:

	# Downloads the dependencies
	make setup
	
	# Runs the  
	make run

### Improvements ###

1. One obvious improvement is that one can't interact with the web service at the moment beyond receiving the final results by requesting the `/averages` route. Should the S3 bucket had more student data added one would need to re-invoke the entire program to reprocess it.

2. The fan-in method of sending the student JSON objects to the main process is risky as it spawns 1 new process per student file. It would be more ideal to have a set worker queue which would limit the number of processes spawned. 

3. While 1 million files isn't much, should the data explode in size the application would need to be refactored in order to be hosted on multiple ec2 instances to increase throughput.