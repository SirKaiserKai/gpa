package student

import "github.com/sirkaiserkai/gpa/average"

// Student wraps a student's data.
type Student struct {
	Name   string  `json:"name"`
	GPA    float64 `json:"gpa"`
	Gender string  `json:"gender"`
	Major  string  `json:"major"`
}

// ToAvgGPA converts a Student struct to an AvgGPA struct. It takes a KeyType
// parameter which determines what the AvgGPA's name equals.
func (s Student) ToAvg(key average.KeyType) average.Average {
	avg := average.Average{
		AvgGPA:     s.GPA,
		TupleCount: 1,
		Type:       string(key),
	}

	switch key {
	case average.Gender:
		avg.Key = s.Gender
	case average.Major:
		avg.Key = s.Major
	default:
		avg.Key = "total"
	}

	return avg
}
