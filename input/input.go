package input

import (
	"encoding/json"
	"io/ioutil"
	"sync"

	"github.com/sirkaiserkai/gpa/input/s3"
	"github.com/sirkaiserkai/gpa/student"
)

// DataSource determines the source of the student data.
type DataSource string

const (
	File DataSource = "file"
	S3   DataSource = "S3"
)

// MainInputChannel encapsulates the functionality with the external source of
// data.
type MainInputChannel struct {
	C             chan student.Student
	StudentSource DataSource
	s3Bucket      string
	folder        string
}

// NewMainInputChannel returns a new MainInputChannel which will channel the
// student data.
func NewMainInputChannel(source DataSource, bucket string, folder string) MainInputChannel {
	return MainInputChannel{
		C:             make(chan student.Student),
		StudentSource: source,
		s3Bucket:      bucket,
		folder:        folder,
	}
}

// NewInputChannel returns a new channel which returns student structs.
func (m *MainInputChannel) NewChildInputChannel() chan student.Student {
	c := make(chan student.Student)

	return c
}

// PullStudents retrieves the student data from an external source.
func (m *MainInputChannel) PullStudents() {
	switch m.StudentSource {
	case File:
		m.pullStudentsFromFile()
	default:
		m.pullStudentsFromS3()
	}
}

// pullStudentsFromS3 is a helper function which streams student data from an S3
// bucket.
func (m *MainInputChannel) pullStudentsFromS3() {
	defer close(m.C)

	s := s3.NewS3Manager()

	// TODO: Get contents list of s3 bucket.
	l, err := s.SeeContents(m.s3Bucket, m.folder)
	if err != nil {
		panic(err)
	}

	var wg sync.WaitGroup
	for _, file := range l {
		wg.Add(1)
		go func(bucket, file string, c chan student.Student) {
			stud, err := s.PullStudent(m.s3Bucket, file)
			if err != nil {
				panic(err)
			}

			m.C <- *stud
			wg.Done()
		}(m.s3Bucket, file, m.C)
	}
	wg.Wait()
}

// pullStudentsFromFile is a helper function for PullStudents which reads the
// contents of a file for the student data.
func (m *MainInputChannel) pullStudentsFromFile() {
	defer close(m.C)

	list, err := readJSONFile(m.folder)
	if err != nil {
		panic(err)
	}

	for _, s := range list {
		m.C <- s
	}
}

func readJSONFile(filename string) ([]student.Student, error) {
	plan, err := ioutil.ReadFile(filename)
	if err != nil {
		return nil, err
	}
	var data []student.Student
	err = json.Unmarshal(plan, &data)
	if err != nil {
		return nil, err
	}

	return data, nil
}

// Sample Data
var StudentList = []student.Student{
	student.Student{
		Name:   "Kai",
		GPA:    3.0,
		Gender: "Male",
		Major:  "Computer Science",
	},
	student.Student{
		Name:   "John",
		GPA:    1.3,
		Gender: "Female",
		Major:  "Computer Science",
	},
	student.Student{
		Name:   "Scott",
		GPA:    1.4,
		Gender: "Male",
		Major:  "Economics",
	},
	student.Student{
		Name:   "Webb",
		GPA:    1.2,
		Gender: "Male",
		Major:  "Economics",
	},
}
