package s3

import (
	"encoding/json"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/s3"
	"github.com/aws/aws-sdk-go/service/s3/s3manager"
	"github.com/sirkaiserkai/gpa/student"
)

// S3Manager wraps the S3 session info info.
type S3Manager struct {
	sess       *session.Session
	downloader *s3manager.Downloader
}

// NewS3Manager returns a new S3Manager instance.
func NewS3Manager() *S3Manager {

	sess := session.Must(session.NewSession())

	return &S3Manager{sess: sess, downloader: s3manager.NewDownloader(sess)}
}

// SeeContents returns a list of the keys for each object found in a bucket and
// filepath.
func (s *S3Manager) SeeContents(bucket, folder string) ([]string, error) {
	svc := s3.New(s.sess)

	params := &s3.ListObjectsInput{
		Bucket: aws.String(bucket),
		Prefix: aws.String(folder),
	}

	resp, err := svc.ListObjects(params)
	if err != nil {
		return nil, err
	}

	l := make([]string, len(resp.Contents))
	for i, key := range resp.Contents {
		l[i] = *key.Key
	}

	return l, nil
}

// PullStudent queries an S3 bucket for a student JSON file.
func (s *S3Manager) PullStudent(bucket, file string) (*student.Student, error) {

	b := aws.NewWriteAtBuffer([]byte{})
	_, err := s.downloader.Download(b, &s3.GetObjectInput{
		Bucket: aws.String(bucket),
		Key:    aws.String(file),
	})
	if err != nil {
		return nil, err
	}

	var stud student.Student
	if err := json.Unmarshal(b.Bytes(), &stud); err != nil {
		return nil, err
	}

	return &stud, err
}
