package average

// KeyType is used to determine an AvgGPA's name field.
type KeyType string

const (
	Gender KeyType = "gender"
	Major  KeyType = "major"
	Total  KeyType = "total"
)

var KeyTypes = []KeyType{Gender, Major, Total}

// Average holds is a struct that holds the average GPA of a set of students.
type Average struct {
	Key        string  `json:"key"`
	AvgGPA     float64 `json:"avg_gpa"`
	TupleCount int     `json:"tuple_count"`
	Type       string  `json:"type"`
}
