FROM golang

ENV AWS_REGION="us-east-1"
ENV AWS_ACCESS_KEY_ID="access_key"
ENV AWS_SECRET_ACCESS_KEY="secret_access_key"

ADD . /go/src/github.com/sirkaiserkai/gpa
WORKDIR /go/src/github.com/sirkaiserkai/gpa

RUN make setup

EXPOSE 8082

CMD make run 