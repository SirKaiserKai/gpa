package main

import (
	"flag"
	"net/http"
	"sync"

	_ "github.com/chrislusf/glow/driver"
	"github.com/chrislusf/glow/flow"

	"github.com/sirkaiserkai/gpa/average"
	"github.com/sirkaiserkai/gpa/input"
	"github.com/sirkaiserkai/gpa/student"

	"github.com/labstack/echo"
)

var (
	// commandline argument flags
	source = flag.String("source", "file", "Data input source ('S3' or 'file' for local files)")
	bucket = flag.String("bucket", "", "S3 bucket to retrieve the student JSON objects from.")
	folder = flag.String("folder", "students", "folder where the student JSON files are stored.")
	port   = flag.String("port", "8082", "port results will be output to (default: 8082).")

	// glow variables and associated channels
	master   *flow.Dataset
	terminal = make(chan average.Average)
	stop     = make(chan bool)

	// studentChan is the input channel for student data.
	studentChan = make(chan student.Student)
)

func init() {
	// Create a flow
	f := flow.New()

	// Create input sources.
	source := f.Channel(studentChan)

	// master consists of a chain of functions which computes data and sends
	// it to the next function (For easier visualization see graph image).
	master = source.
		Partition(3).
		// Converts Student struct to Average struct and passes it into the
		// chan which feeds into the next function.
		Map(func(s student.Student, avgChan chan average.Average) {
			for _, key := range average.KeyTypes {
				avgChan <- s.ToAvg(key)
			}
		}).
		// returns key and Average struct so that it can be passed into
		// ReduceByKey function.
		Map(func(avg average.Average) (string, average.Average) {
			return avg.Key, avg
		}).
		// Reduces the passed in (string, Average) tuples into individual
		// Average structs. Final output will consist of one Average struct per
		// key value (e.g. "Computer Science", "Total", "Male", etc).
		ReduceByKey(func(avg0, avg1 average.Average) average.Average {
			avg0.AvgGPA += avg1.AvgGPA
			avg0.TupleCount += avg1.TupleCount

			return avg0
		}).
		// Calculates the average GPA of each total sum.
		Map(func(key string, avg average.Average) (string, average.Average) {
			avg.AvgGPA = avg.AvgGPA / float64(avg.TupleCount)
			return avg.Key, avg
		}).
		// Passes the now successfully process Average structs to the terminal
		// channel.
		Map(func(key string, avg average.Average) {
			terminal <- avg
		})
}

func main() {
	flag.Parse() // Parses input flags if provided.
	flow.Ready()

	inputChan := input.NewMainInputChannel(input.DataSource(*source), *bucket, *folder)
	inputChan.C = studentChan

	// Runs the PullStudents method in seperate goroutine (process).
	go inputChan.PullStudents()

	// Runs the data processing in a seperate go routine to open the main
	// process to handle the terminal channel's output.
	go func() {
		master.Run()
		stop <- true // Once finished send stop signal back to main process
	}()

	// output map (dictionary in python terms) with an associated mutex m.
	m := sync.Mutex{}
	output := make(map[string]interface{})

	// Continuous loop
	go func() {
		for {
			select {
			case avg := <-terminal:
				// Received average
				m.Lock()
				output[avg.Key] = avg
				m.Unlock()
			case <-stop:
				// Received stop signal exit.
				return
			}
		}
	}()

	// Simple server that hosts the processing results.
	e := echo.New()
	e.GET("/averages", func(c echo.Context) error {
		m.Lock()
		resp := c.JSON(http.StatusOK, output)
		m.Unlock()

		return resp
	})
	e.Logger.Fatal(e.Start(":" + *port))
}
