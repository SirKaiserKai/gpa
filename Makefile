.PHONY: run setup docker-build docker-run

run:
	go run main.go -bucket="images-12312ajdklasjdjksajdj1231" -source=S3 -folder=students/output

setup: 
	go get github.com/chrislusf/glow
	go get github.com/labstack/echo
	go get github.com/aws/aws-sdk-go

docker-build:
	docker build -t gpa .

docker-run:
	docker run -it --publish 8082:8082 --rm --name my-running-app gpa